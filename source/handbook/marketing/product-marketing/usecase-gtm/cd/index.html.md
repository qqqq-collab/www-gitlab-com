---
layout: markdown_page
title: "Usecase: Continuous Delivery"
noindex: true
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

#### Who to contact

| Product Marketing | Technical Marketing |
| ---- | --- |
| @parker_ennis  | @iganbaruch |

## Continuous Delivery

placeholder for additional info

[GitLab CI/CD](/stages-devops-lifecycle/continuous-integration/) comes built-in to GitLab's complete DevOps platform delivered as a single application. There's no need to cobble together multiple tools and users get a seamless experience out-of-the-box.

## Personas

### User Persona

The typical **user personas** for this use case are...

### Buyer Personas

The typical **buyer personas** for this use case are...

## Industry Analyst Resources

Examples of comparative research for this use case are listed just below. Additional research relevant to this use case can be found in the [Analyst Reports - Use Cases](https://docs.google.com/spreadsheets/d/1vXpniM08Ql0v0yDd22pcNmXpDrA-NInJOwj25PRuHXA/edit?usp=sharing) spreadsheet.

## Market Requirements

| Market Requirement | Description | Typical capability-enabling features | Value/ROI |
|---------|-------------|-----------|------|
| 1)  |  |  |  |
| 2)  |  |  |  |
| 3)  |  |  |  |
| 4)  |  |  |  |
| 5)  |  |  |  |
| 6)  |  |  |  |
| 7)  |  |  |  |
| 8)  |  |  |  |
| 9)  |  |  |  |
| 10) |  |  |  |
| 11) |  |  |  |


## How GitLab Meets the Market Requirements

A collection of short demonstrations that show GitLab's CD capabilities.

* placeholder


## GitLab Stages and Categories

At GitLab, we address these market requirements through features included in these stages/categories:

[**Release**](/stages-devops-lifecycle/release/)
* Continuous Delivery (CD), Pages, Review Apps, Incremental Rollout, Feature Flags, Release Orchestration, Release Evidence, Secrets Management

* tbd

## Top GitLab Features for CD

* tbd

## Top 3 GitLab Differentiators

| Differentiator |  Value  |  Proof Point |
|----------|-------------|------|
| 1)  |   |  |
| 2)  |   |  |
| 3)  |   |  |

## Message house

tbd

The message house provides a structure to describe and discuss the value and differentiators for Continuous Delivery with GitLab.

## Customer Facing Slides

-

### Discovery Questions

-

#### Sample Discovery Questions

-

#### Additional Discovery Questions

-

### Industry Analyst Relations (IAR) Plan

- The IAR Handbook page has been updated to reflect our plans for [incorporating Use Cases into our analyst conversations](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/#how-we-incorporate-use-cases-into-our-analyst-conversations).
- For  details specific to each use case, and in respect of our contractual confidentiality agreements with Industry Analyst firms, our engagement plans are available to GitLab team members in the following protected document: [IAR Use Case Profile and Engagement Plan](https://docs.google.com/spreadsheets/d/14UthNcgQNlnNfTUGJadHQRNZ-IrAe6T7_o9zXnbu_bk/edit#gid=0).

For a list of analysts with a current understanding of GitLab's capabilities for this use case, please reach out to Analyst Relations via Slack (#analyst-relations) or by submitting an [issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new) and selecting the "AR-Analyst-Validation" template.

## Competitive Comparison

## Proof Points - Customer Recognitions

### Quotes and reviews

#### Gartner Peer Insights

*Gartner Peer Insights reviews constitute the subjective opinions of individual end users based on their own experiences, and do not represent the views of Gartner or its affiliates. Obvious typos have been amended.*


### Blogs

#### TBD

* **Problem:**
* **Solution:**
* **Result:**
* **Sales Segment:**

#### TBD

* **Problem:**
* **Solution:**
* **Result:**
* **Sales Segment:**


### Case Studies

#### TBD

* **Problem** 
* **Solution:**
* **Result:**
* **Sales Segment:**

#### TBD 

* **Problem**
* **Solution:**
* **Result:**
* **Sales Segment:**


### References to help you close

## Adoption Guide

The following section provides resources to help TAMs lead capabilities adoption, but can also be used for prospects or customers interested in adopting GitLab stages and categories.

### Playbook Steps

1. Ask Discovery Questions to identify customer need
2. Complete the deeper dive discovery sharing demo, proof points, value positioning, etc.
3. Deliver [pipeline conversion workshop](https://about.gitlab.com/handbook/customer-success/playbooks/ci-verify.html) and user enablement example
4. Agree to adoption roadmap, timeline and change management plans, offering relevant services (as needed) and updating the success plan (as appropriate)
5. Lead the adoption plan with the customer, enabling teams and tracking progress through engagement and/or telemetry data showing use case adoption

### Adoption Recommendation

placeholder for a table showing the recommended use cases to adopt CD, links to product documentation, the respective subscription tier for the use case, and telemetry metrics.

#### Additional Documentation Links

- [Introduction to CI/CD with GitLab](https://docs.gitlab.com/ee/ci/introduction/)
- [Getting started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/)
- [GitLab CI/CD Examples](https://docs.gitlab.com/ee/ci/examples/)

### Enablement and Training

The following will link to enablement and training videos and content.

- [Make Your Life Easier with CI/CD Presentation](https://docs.google.com/presentation/d/1scYkmV4Xdfj-8iwwpEiLCe0vBfpAdrL5pyA2w1Fgnf0/edit#slide=id.g7193b194b5_0_96)
- [CI/CD Overview Video](https://www.youtube.com/watch?v=wsbSvLyC2Z8)
- [CS Skills Exchange: CI Deep Dive](https://www.youtube.com/watch?v=ZVUbmVac-m8&list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_&index=3&t=0s)
- [CS Skills Exchange: Runners Overview](https://www.youtube.com/watch?v=JFMXe1nMopo&list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_&index=11&t=0s)
- [CS Skills Exchange: Runners Overview](https://www.youtube.com/watch?v=JFMXe1nMopo&list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_&index=11&t=0s)
- [Technically Competing Against Microsoft Azure DevOps](https://drive.google.com/open?id=18jwSeeUylGXv8LoEedCMRfBZt9t7QLOYKCHJp-SvdqA) *(GitLab internal only)*
- [Competing Against Jenkins](https://drive.google.com/open?id=1IvftLfaQyKn5-n1GLgCZokOoLU-FFzQ8LfJ9cf0FVeg) *(GitLab internal only)*
- *Coming soon.... CD Learning Path*

### Professional Service Offers


## Key Value (at tiers)

### Premium/Silver
- Describe the value proposition of why Premium/Silver for this Use Case

### Ultimate/Gold
- Describe the value proposition of why Ultimate/Gold for this Use Case

## Resources

### What is CI/CD?

Check out this introductory video to learn the basics of CI/CD as software development best practices and how they apply with GitLab CI/CD!
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/nLwJtVWXN70" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Presentations
* [Why CI/CD?](https://docs.google.com/presentation/d/1OGgk2Tcxbpl7DJaIOzCX4Vqg3dlwfELC3u2jEeCBbDk)

### Continuous Delivery Videos
* [CI/CD with GitLab](https://youtu.be/1iXFbchozdY)
* [GitLab for complex CI/CD: Robust, visible pipelines](https://youtu.be/qy8A7Vp_7_8)
* [How do Runners work?](https://youtu.be/IsthhMm64u8)

### Integrations Demo Videos
* [Migrating from Jenkins to GitLab](https://youtu.be/RlEVGOpYF5Y)
* [Using GitLab CI/CD pipelines with GitHub repositories](https://youtu.be/qgl3F2j-1cI)

### Clickthrough & Live Demos
* [Live Demo: GitLab CI/CD Deep Dive](https://youtu.be/pBe4t1CD8Fc)
